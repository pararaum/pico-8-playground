#! /usr/bin/env python3

import argparse
import string
import PIL
import PIL.Image

PICO8PALETTE = [
    #Palette Name: PICO-8
    #Description: The <a href="https://www.lexaloffle.com/pico-8.php">PICO-8</a> is a virtual video game console created by Lexaloffle Games.
    (0      , 0     , 0     ), #000000
    (29     , 43    , 83    ), #1D2B53
    (126    , 37    , 83    ), #7E2553
    (0      , 135   , 81    ), #008751
    (171    , 82    , 54    ), #AB5236
    (95     , 87    , 79    ), #5F574F
    (194    , 195   , 199   ), #C2C3C7
    (255    , 241   , 232   ), #FFF1E8
    (255    , 0     , 77    ), #FF004D
    (255    , 163   , 0     ), #FFA300
    (255    , 236   , 39    ), #FFEC27
    (0      , 228   , 54    ), #00E436
    (41     , 173   , 255   ), #29ADFF
    (131    , 118   , 156   ), #83769C
    (255    , 119   , 168   ), #FF77A8
    (255    , 204   , 170   )  #FFCCAA
]

def find_pal_idx(tup):
    idx = -1
    mindelta = -1
    for cidx, pico in enumerate(PICO8PALETTE):
        delta = sum(abs(i-j) for i,j in zip(tup, pico))
        if delta < mindelta or idx == -1:
            idx = cidx
            mindelta = delta
    assert(idx > -1)
    return idx

def run(fname):
    """Run for a single file

    @param fname: filename
    @return: image data as list, top to bottom, left to right
"""
    img = PIL.Image.open(fname)
    # Get palette for image and convert to PICO-8 index.
    pal = img.getpalette()
    pal = [(pal[i], pal[i+1], pal[i+2]) for i in range(0, len(pal), 3)]
    pal = [i for i in map(find_pal_idx, pal)]
    imgdat = []
    for y in range(img.height):
        imgdat.extend(pal[img.getpixel((x, y))] for x in range(img.width))
    return imgdat

def main():
    """Main function

    Parses the command line and starts the conversion.
"""
    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs='+', help="list of images to convert")
    args = parser.parse_args()
    for curr in args.files:
        imgstr = ''.join("%x" % i for i in run(curr))
        varnam = ''.join(filter(str.isalnum, curr))
        print("%s=\"%s\"" % (varnam, imgstr))

if __name__ == "__main__":
    main()
